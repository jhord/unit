all: unit unit-test

unit: unit.o
	cc -Wall -o unit -O2 unit.o

unit.o: unit.c
	cc -Wall -o unit.o -O2 -c unit.c

unit-test: unit-test.o
	cc -Wall -o unit-test -O2 unit-test.o

unit-test.o: unit-test.c
	cc -Wall -o unit-test.o -O2 -c unit-test.c

clean:
	rm -f unit unit.o unit-test unit-test.o
