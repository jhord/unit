#include <err.h>
#include <fcntl.h>
#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define VERSION  "0.0.0"
#define DEV_NULL "/dev/null"

struct app_t {
  int wait;
  int verbose;
};

int  version(void);
int  usage(char * const, const char *);
int  to_int(const char *);
void set_file_buffers(void);
int  loop(struct app_t *, char * const []);
void hatch(struct app_t *, char * const []);
void spawn(struct app_t *, const char *, char * const []);
void on_sig_chld(int);

int
main(int argc, char * const argv[]) {
  int           opt;
  struct app_t  app;
  struct option options[] = {
    { "verbose", no_argument,       NULL, 'v' },
    { "version", no_argument,       NULL, 'V' },
    { "wait",    required_argument, NULL, 'w' },
    { "help",    no_argument,       NULL, '?' },
    { NULL,      0,                 NULL,  0  }
  };

  memset(&app, 0, sizeof(app));

  app.wait = 1;

  while((opt = getopt_long(argc, argv, "vVw:?", options, NULL)) != EOF) {
    switch(opt) {
    case 'w':
      app.wait = to_int(optarg);
      break;

    case 'v':
      if(0 == app.verbose) {
        set_file_buffers();
        app.verbose = 1;
      }
      break;

    case 'V':
      exit(version());
      break;

    case '?':
    default:
      exit(usage(argv[0], NULL));
      break;
    }
  }

  if(optind >= argc) {
    exit(usage(argv[0], "No command given"));
  }

  exit(loop(&app, &argv[optind]));
}

int
version(void) {
  printf("unit v%s\n", VERSION);
  return 0;
}

int
usage(char * const exe, const char * text) {
  if(text != NULL) {
    printf("%s\n\n", text);
  }
  printf(
    "usage: %s [options] command [arg [arg ...]]\n"
    "options:\n"
    "  -v, --verbose  Turn on verbose messages\n"
    "  -w, --wait     Specify the minimum amount of time (in seconds) to wait\n"
    "                 between command executions\n"
    "other:\n"
    "  -V, --version  Print the program version and exit\n"
    "  -?, --help     You are here\n",
    exe
  );
  return 0;
}

int
to_int(const char * from) {
  int to;

  to = atoi(from);
  return (to < 0) ? 1 : to;
}

void
set_file_buffers(void) {
  int rc;

  rc = setvbuf(stdout, NULL, _IONBF, 0);
  if(rc != 0) {
    err(1, "stdout: setvbuf");
  }

  rc = setvbuf(stderr, NULL, _IONBF, 0);
  if(rc != 0) {
    err(1, "stderr: setvbuf");
  }

  return;
}

int
loop(struct app_t * app, char * const argv[]) {
  int    rest;
  time_t now;
  time_t then;

  while(1) {
    then = time(NULL);

    hatch(app, argv);

    if(app->wait > 0) {
      now  = time(NULL);
      rest = app->wait - (now - then);
      if(rest > 0) {
        if(app->verbose) {
          printf("unit: loop: waiting %d %s\n", rest, (1 == rest ? "second" : "seconds"));
        }
        sleep(rest);
      }
    }

    if(1 == getppid()) {
      break;
    }
  }

  return 0;
}

void
hatch(struct app_t * app, char * const argv[]) {
  int   wstatus;
  pid_t pid;
  pid_t wrc;

  pid = fork();
  if(pid < 0) {
    err(1, "unit: fork");
    return;
  }

  if(pid > 0) {
    if(app->verbose) {
      printf("unit: hatch: child process fork: %d\n", pid);
    }

    wrc = waitpid(pid, &wstatus, 0);
    if(-1 == wrc) {
      err(1, "waitpid");
    }

    if(app->verbose) {
      printf("unit: hatch: child process exit: %d status=%d", pid, WEXITSTATUS(wstatus));
      if(WIFSIGNALED(wstatus)) {
        printf(" signal=%d", WTERMSIG(wstatus));
      }
      if(WCOREDUMP(wstatus)) {
        printf(" core");
      }
      putchar('\n');
    }
    return;
  }

  spawn(app, argv[0], argv);
}

void
spawn(struct app_t * app, const char * file, char * const argv[]) {
  int rc;

  if(app->verbose) {
    printf("unit: spawn: %s", file);
    for(int i = 1; argv[i] != NULL; i++) {
      printf(" %s", argv[i]);
    }
    putchar('\n');
  }

  rc = execvp(file, argv);
  if(-1 == rc) {
    err(1, "exec: %s", file);
  }

  errx(1, "exec: %s: %d", file, rc);
}
