#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VERSION "0.0.0"

int version(void);
int usage(const char *);
int to_int(const char *);

int
main(int argc, char * const argv[]) {
  int           opt;
  int           status;
  int           wait;
  int           verbose;
  struct option options[] = {
    { "status",   required_argument, NULL, 's' },
    { "verbose",  no_argument,       NULL, 'v' },
    { "wait",     required_argument, NULL, 'w' },
    { "version",  no_argument,       NULL, 'V' },
    { "help",     no_argument,       NULL, '?' },
    { NULL,       0,                 NULL,  0 }
  };

  status  = 0;
  verbose = 0;
  wait    = 0;

  while((opt = getopt_long(argc, argv, "s:vVw:?", options, NULL)) != EOF) {
    switch(opt) {
    case 's':
      status = to_int(optarg);
      break;

    case 'w':
      wait = to_int(optarg);
      break;

    case 'v':
      verbose = 1;
      break;

    case 'V':
      exit(version());
      break;

    case '?':
    default:
      exit(usage(argv[0]));
      break;
    }
  }

  if(verbose) {
    printf("unit-test: argc=%d (%d)\n", argc, (argc - optind));
    for(int i = optind; i < argc; i++) {
      if(argc <= 10) {
        printf("  argv[%d]: %s\n", i, argv[i]);
      }
      else {
        printf("  argv[%02d]: %s\n", i, argv[i]);
      }
    }
  }

  if(wait > 0) {
    if(verbose) {
      printf("unit-test: waiting %d %s\n", wait, (1 == wait ? "second" : "seconds"));
    }
    sleep(wait);
  }

  if(verbose) {
     printf("unit-test: exiting; status=%d\n", status);
  }

  exit(status);
}

int
version(void) {
  printf("unit-test v%s\n", VERSION);
  return 0;
}

int
usage(const char * exe) {
  printf(
    "usage: %s [options] [arg [arg ...]]"
    "options:\n"
    "  -s, --status   Exit with the specified status code\n"
    "  -v, --verbose  Turn on verbose messages\n"
    "  -w, --wait     Wait the specified number of seconds before exiting\n"
    "other:\n"
    "  -V, --version  Print the program version and exit\n"
    "  -?, --help     You are here\n",
    exe
  );
  return 0;
}

int
to_int(const char * from) {
  int to;

  to = atoi(from);
  return to < 0 ? 0 : to;
}
